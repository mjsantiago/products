<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Product extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            
            'product_id'=> 'sometimes|required',
            'product_name'=> 'sometimes|required',
            'product_desc'=> 'sometimes|required',
            'category_id'=> 'sometimes|required'
        ];
    }
}
