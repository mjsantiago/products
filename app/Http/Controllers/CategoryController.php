<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Services\CategoryService;
use App\Category;


class CategoryController extends Controller
{
    private $service;

    public function __construct(CategoryService $service){
        $this->service = $service;
    }

    public function index()
    {
        return $this->service->all();
    }

    public function store(CategoryRequest $request)
    {
        $request->validated();
        return $this->service->store($request->toArray());
    }

    public function show($id)
    {
        return $this->service->show($id);
    }

    public function update(Request $request, $id)
    {
        $request->validated();
        return $this->service->update($request->toArray(),$id);
    }

    public function destroy($id)
    {
        return $this->service->delete($id);
    }
}
