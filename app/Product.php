<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes; 

class Products extends Model
{
    //use SoftDeletes;
    protected $table = 'products';

    protected $fillable = [
        'product_id',
         'product_name',
         'product_desc',
         'category_id'
    ];
}
