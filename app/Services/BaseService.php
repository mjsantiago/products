<?php

namespace App\Services;
use App\MakeResponse;

abstract class BaseService
{
    protected $response = ['status' => '', 'message' => '', 'data' => null];
    protected $http_code;

    abstract public function all();

    abstract public function store(array $data);

    abstract public function update(array $data, $id);

    abstract public function delete($id);

    abstract public function show($id);

    protected function sendResponse($status, $message, $data, $http_code)
    {
        $response = MakeResponse::petnetResponse($status, $message, $data, $http_code);

        return $response;
    }
}
