<?php

namespace App\Services;

use App\Category;
use App\MakeResponse;

class ProductService extends BaseService
{

    public function all(){
        $users = Category::all();
        return $this->sendResponse('000', 'Categories retrieved.',$users->toArray(),200);
    }

    public function store(array $data){
        $category = Category::create($data);
        if($category){
            return $this->sendResponse('000','Categories added',$category,200);
        }
        else{
            return $this->sendResponse('001','Error adding Categories.',null,422);
        }
    }

    public function update(array $data, $id){
        
        $category = Category::findOrFail($id);
        $category->update($data);

        return $this->sendResponse('000','Categories updated',$category,200);
    }

    public function delete($id){
        $category = Category::findOrFail($id);
        $category->delete($id);
        return $this->sendResponse('000','Categories deleted',$category,200);
    }

    public function show($id){
        $category = Category::findOrFail($id);
        return $this->sendResponse('000','Categories retrieved.',$category,200);
    }
}
