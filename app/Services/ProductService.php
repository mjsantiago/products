<?php

namespace App\Services;

use App\Product;
use App\MakeResponse;

class ProductService extends BaseService
{

    public function all(){
        $users = Product::all();
        return $this->sendResponse('000', 'Products retrieved.',$users->toArray(),200);
    }

    public function store(array $data){
        $product = Product::create($data);
        if($product){
            return $this->sendResponse('000','Products added',$product,200);
        }
        else{
            return $this->sendResponse('001','Error adding Products.',null,422);
        }
    }

    public function update(array $data, $id){
        
        $product = Product::findOrFail($id);
        $product->update($data);

        return $this->sendResponse('000','Products updated',$product,200);
    }

    public function delete($id){
        $product = Product::findOrFail($id);
        $product->delete($id);
        return $this->sendResponse('000','Products deleted',$product,200);
    }

    public function show($id){
        $product = Product::findOrFail($id);
        return $this->sendResponse('000','Products retrieved.',$product,200);
    }
}
